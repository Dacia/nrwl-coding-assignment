import { Injectable, OnDestroy } from '@angular/core';
import { LoadingState, TicketsStore } from './tickets.store';
import { BackendService, Ticket, User } from '../../backend.service';
import { pipeFuncs } from '../../app.utils';
import { addTicketReducer, updateTicketReducer } from './tickets.reducers';

@Injectable()
export class TicketsService implements OnDestroy {

    constructor(private store: TicketsStore, private backendService: BackendService) {}

    ngOnDestroy() {
    }

    setLoadingState(loadingState: LoadingState) {
        this.store.update((state) => {
            return {
                ...state,
                loadingState
            }
        })
    }

    setInitialData(tickets: Ticket[], users: User[]) {
        this.store.update((state) => {
            return {
                ...state,
                tickets,
                users,
                loadingState: 'ok'
            }
        })
    }

    setTickets(tickets: Ticket[]) {
        this.store.update((state) => {
            return {
                ...state,
                tickets
            }
        })
    }

    setUsers(users: User[]) {
        this.store.update((state) => {
            return {
                ...state,
                users
            }
        })
    }

    setFilterValue(filter: string) {
        this.store.update((state) => {
            return {
                ...state,
                filter
            }
        })
    }

    setCurrentTicketId(ticketId: number) {
        this.store.update((state) => {
            return {
                ...state,
                currentTicketId: ticketId
            }
        })
    }

    addTicket(ticketDescription: string) {
        this.store.update((state) => ({...state, isAddingTicket: true}))
        this.backendService.newTicket({description: ticketDescription}).subscribe(
            (ticketAdded) => {
                this.store.update(pipeFuncs(
                    (state) => ({...state, isAddingTicket: false}),
                    addTicketReducer(ticketAdded)
                ))
            },
            (error) => {
                this.store.update(
                    (state) => ({...state, isAddingTicket: false})
                )
            }
        )
    }

    completeTicket(ticketId: number) {
        this.store.update((state) => ({...state, isCompletingTicket: true}));
        this.backendService.complete(ticketId, true).subscribe(
            (ticketCompleted) => {
                this.store.update(pipeFuncs(
                    (state) => ({...state, isCompletingTicket: false}),
                    updateTicketReducer(ticketCompleted)
                ))
            },
            (error) => {
                this.store.update(
                    (state) => ({...state, isCompletingTicket: false})
                )
            }
        )
    }

    assignTicket(ticketId: number, userId: number) {
        this.store.update((state) => ({...state, isAssigningTicket: true}));
        this.backendService.assign(ticketId, userId).subscribe(
            (ticketAssigned) => {
                this.store.update(pipeFuncs(
                    (state) => ({...state, isAssigningTicket: false}),
                    updateTicketReducer(ticketAssigned)
                ))
            },
            (error) => {
                this.store.update(
                    (state) => ({...state, isAssigningTicket: false}),
                )
            }
        )
    }

}
