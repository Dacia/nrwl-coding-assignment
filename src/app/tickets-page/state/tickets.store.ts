import { Injectable, OnDestroy } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { Ticket, User } from '../../backend.service';

export type LoadingState = 'error' | 'loading' | 'ok';

export interface TicketsState {
    users: User[];
    tickets: Ticket[];
    currentTicketId: number | null;
    filter: string;
    loadingState: LoadingState;
    isAddingTicket: boolean;
    isCompletingTicket: boolean;
    isAssigningTicket: boolean;
}

function createInitialState(): TicketsState {
    return {
        users: [],
        tickets: [],
        currentTicketId: null,
        filter: '',
        loadingState: 'loading',
        isAddingTicket: false,
        isCompletingTicket: false,
        isAssigningTicket: false
    };
}

@Injectable()
@StoreConfig({name: 'tickets'})
export class TicketsStore extends Store<TicketsState> implements OnDestroy {

    constructor() {
        super(createInitialState());
    }

    ngOnDestroy() {
        this.destroy();
    }
}

