import { TicketsState } from './tickets.store';
import { Ticket } from '../../backend.service';

export function addTicketReducer(ticket: Ticket) {
    return (state: TicketsState): TicketsState => {
        return {
            ...state,
            tickets: [...state.tickets, ticket]
        };
    };
}

export function updateTicketReducer(tickedUpdated: Ticket) {
    return (state: TicketsState): TicketsState => {
        const ticketToUpdateIndex = state.tickets.findIndex(ticket => ticket.id === tickedUpdated.id);
        if (ticketToUpdateIndex < 0) {
            return state;
        }

        // Make a shallow copy of the tickets and update the ticket that needs to be updated.
        const ticketsCopy = [...state.tickets];
        ticketsCopy[ticketToUpdateIndex] = tickedUpdated

        return {
            ...state,
            tickets: ticketsCopy
        };
    };
}

