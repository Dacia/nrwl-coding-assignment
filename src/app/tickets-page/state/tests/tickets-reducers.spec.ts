import { Ticket } from '../../../backend.service';
import { TicketsState } from '../tickets.store';
import { updateTicketReducer } from '../tickets.reducers';


const getTestTickets = (): Ticket[] => ([
    {
        id: 0,
        description: 'Install a monitor arm',
        assigneeId: 111,
        completed: false
    },
    {
        id: 1,
        description: 'Move the desk to the new location',
        assigneeId: 111,
        completed: false
    }
]);

const generateTestTicketsState = (partialState: Partial<TicketsState>): TicketsState => ({
    tickets: [],
    users: [],
    currentTicketId: null,
    loadingState: 'ok',
    isCompletingTicket: false,
    isAddingTicket: false,
    isAssigningTicket: false,
    filter: '',
    ...partialState
});

describe('Tickets Reducers tests', () => {

    describe('Update ticket reducer', () => {

        it('should update ticket with id 0 changing the property completed from false to true', () => {
            const prevState = generateTestTicketsState({tickets: getTestTickets()});
            const newState = updateTicketReducer({
                ...prevState.tickets[0],
                completed: true
            })(prevState);
            // since state is small, check ALL state, this way we also check for possible erroneous side effects on other properties.
            expect(newState).toEqual({
                tickets: [
                    {id: 0, description: 'Install a monitor arm', assigneeId: 111, completed: true}, // only should have change ticket id 0 completed property from false to true.
                    {id: 1, description: 'Move the desk to the new location', assigneeId: 111, completed: false}
                ],
                users: [],
                currentTicketId: null,
                loadingState: 'ok',
                isCompletingTicket: false,
                isAddingTicket: false,
                isAssigningTicket: false,
                filter: ''
            })
        })

        it('should not update any ticket because ticket with id 100 does not exist', () => {
            const ticket: Ticket = {
                id: 100,
                description: 'test',
                assigneeId: 111,
                completed: false
            };
            const prevState = generateTestTicketsState({tickets: getTestTickets()});
            const newState = updateTicketReducer(ticket)(prevState);
            // Check equality for reference! Since the state shouldn't have to be mutated, the reference should be the same.
            expect(newState).toBe(prevState)
        })
    })

})
