import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { Observable } from 'rxjs';
import { LoadingState, TicketsState, TicketsStore } from './tickets.store';
import { Ticket, TicketUser, User } from '../../backend.service';
import { distinctUntilChanged, map } from 'rxjs/operators';

@Injectable()
export class TicketsQuery extends Query<TicketsState> {

    users$: Observable<User[]>;
    tickets$: Observable<Ticket[]>;
    currentTicketId$: Observable<number | null>;
    currentTicket$: Observable<Ticket | null>;
    currentTicketUser$: Observable<TicketUser | null>;
    loadingState$: Observable<LoadingState>;
    isAddingTicket$: Observable<boolean>;
    isCompletingTicket$: Observable<boolean>;
    filter$: Observable<string>;
    filteredTickets$: Observable<Ticket[]>;

    constructor(protected store: TicketsStore) {
        super(store);

        // Log each change of the state (for debug purposes)
        // this.select().subscribe((state) => console.log('tickets state', state));

        this.users$ = this.select(state => state.users);
        this.tickets$ = this.select(state => state.tickets);
        this.currentTicketId$ = this.select(state => state.currentTicketId);

        // This selector could be enhanced if tickets are also stored in a dictionary
        this.currentTicket$ = this.select([state => state.tickets, state => state.currentTicketId]).pipe(
            map(([tickets, currentTicketId]: [Ticket[], number | null]) => {
                return tickets.find(ticket => ticket.id === currentTicketId) || null;
            }),
            distinctUntilChanged(),
        );

        this.currentTicketUser$ = this.select([state => state.users, state => state.tickets, state => state.currentTicketId]).pipe(
            map(([users, tickets, currentTicketId]: [User[], Ticket[], number | null]) => {
                const ticket = tickets.find(ticket => ticket.id === currentTicketId) || null;
                if (!ticket) {
                    return null;
                }

                return {
                    ...ticket,
                    user: users.find(user => user.id === ticket.assigneeId) || null
                }
            }),
            distinctUntilChanged()
        );

        this.loadingState$ = this.select(state => state.loadingState);
        this.isAddingTicket$ = this.select(state => state.isAddingTicket);
        this.isCompletingTicket$ = this.select(state => state.isCompletingTicket);

        this.filter$ = this.select(state => state.filter);
        this.filteredTickets$ = this.select([state => state.filter, state => state.tickets]).pipe(
            map(([filter, tickets]: [string, Ticket[]]) => {
                if (!filter) {
                    return tickets;
                }

                return tickets.filter(ticket => ticket.description.toLowerCase().includes(filter.toLowerCase()));
            })
        );
    }
}
