import { Component, OnDestroy, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { ActivatedRoute, Router } from '@angular/router';
import { distinctUntilChanged, map, take, tap } from 'rxjs/operators';
import { coerceNumberProperty } from '@angular/cdk/coercion';
import { TicketsStore } from './state/tickets.store';
import { TicketsService } from './state/tickets.service';
import { TicketsQuery } from './state/tickets.query';
import { combineLatest, defer, Subscription } from 'rxjs';

@Component({
    selector: 'app-tickets-page',
    templateUrl: './tickets-page.component.html',
    styleUrls: ['./tickets-page.component.scss'],
    providers: [
        TicketsStore,
        TicketsService,
        TicketsQuery
    ]
})
export class TicketsPageComponent implements OnInit, OnDestroy {

    addTicketValue = '';

    private subscriptions: Subscription[] = [];

    constructor(private backend: BackendService,
                private route: ActivatedRoute,
                private router: Router,
                private ticketsService: TicketsService,
                public ticketsQuery: TicketsQuery) { }

    ngOnInit(): void {
        this.subscriptions.push(
            // Get list of tickets
            defer(() => {
                // Set initial loading state to the store.
                this.ticketsService.setLoadingState('loading');
                // Request users and tickets data.
                return combineLatest([
                    this.backend.tickets(),
                    this.backend.users()
                ]).pipe(
                    // Both are cold and emit 1 item and finish, but put the take(1) as a good practice. Without putting the take(1),
                    // it would be perfectly okay, but for newcomers it seems like a permanent stream of data, when it is not.
                    take(1)
                )
            }).subscribe(([tickets, users]) => {
                    this.ticketsService.setInitialData(tickets, users);
                },
                error => {
                    this.ticketsService.setLoadingState('error');
                }
            ),

            this.route.params.pipe(
                map(routeParams => {
                    // Coerce to number, if not possible fallback to null.
                    return coerceNumberProperty(routeParams.id, null)
                }),
                distinctUntilChanged()
            ).subscribe((ticketId: number | null) => {
                if (ticketId == null) {
                    this.ticketsService.setCurrentTicketId(null);
                } else {
                    this.ticketsService.setCurrentTicketId(ticketId);
                }
            })
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }


    onFilterInputChange(event: Event) {
        const value = (event.target as any).value;
        this.ticketsService.setFilterValue(value);
    }

    onTicketClicked(ticket) {
        if (coerceNumberProperty(this.route.snapshot.params.id, null) === ticket.id) {
            // If we re-click the selected ticked, navigate to tickets page
            this.router.navigate(['/tickets'])
        } else {
            // If ticket is clicked navigate to it's detail
            this.router.navigate([`/tickets`, ticket.id], {relativeTo: this.route})
        }
    }

    addTicket() {
        this.ticketsService.addTicket(this.addTicketValue)
        this.addTicketValue = '';
    }

}
