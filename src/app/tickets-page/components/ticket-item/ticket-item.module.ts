import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketItemComponent } from './ticket-item.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
    declarations: [
        TicketItemComponent
    ],
    exports: [
        TicketItemComponent
    ],
    imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule
    ]
})
export class TicketItemModule { }
