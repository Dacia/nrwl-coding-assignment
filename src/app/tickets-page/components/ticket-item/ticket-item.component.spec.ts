import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketItemComponent } from './ticket-item.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

const getMockTicket = () => ({
    id: 0,
    description: 'Install a monitor arm',
    assigneeId: 111,
    completed: false
});

describe('TicketItemComponent', () => {
    let component: TicketItemComponent;
    let fixture: ComponentFixture<TicketItemComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [TicketItemComponent],
            imports: [
                CommonModule,
                MatButtonModule,
                MatIconModule
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TicketItemComponent);
        component = fixture.componentInstance;
        component.ticket = getMockTicket();
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should render the description', () => {
        const p = fixture.nativeElement.querySelector('p');
        // Test elements that depend on the input
        expect(p.textContent).toEqual('Install a monitor arm');
    });
});
