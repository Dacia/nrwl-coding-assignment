import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketDetailComponent } from './ticket-detail.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { BackendService, TicketUser } from '../../../backend.service';
import { TicketsStore } from '../../state/tickets.store';
import { TicketsService } from '../../state/tickets.service';
import { TicketsQuery } from '../../state/tickets.query';

const getMockTicketUser = (): TicketUser => ({
  id: 0,
  description: 'Install a monitor arm',
  assigneeId: 111,
  completed: false
});

describe('TicketDetailComponent', () => {
  let component: TicketDetailComponent;
  let fixture: ComponentFixture<TicketDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TicketDetailComponent ],
      providers: [
        BackendService,
        TicketsStore,
        TicketsService,
        TicketsQuery,
      ],
      imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketDetailComponent);
    component = fixture.componentInstance;
    component.ticketUser = getMockTicketUser();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
