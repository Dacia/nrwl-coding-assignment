import { ChangeDetectorRef, Component, HostBinding, Input, OnInit } from '@angular/core';
import { TicketUser } from '../../../backend.service';
import { TicketsQuery } from '../../state/tickets.query';
import { TicketsService } from '../../state/tickets.service';
import { MatSelectChange } from '@angular/material/select';

@Component({
    selector: 'app-ticket-detail',
    templateUrl: './ticket-detail.component.html',
    styleUrls: ['./ticket-detail.component.scss']
})
export class TicketDetailComponent implements OnInit {
    /** Even so we can perfectly get this value from the store, prefer in this case pass it through input JUST to make it clear that has a 'ticketUser' dependency. */
    @Input() ticketUser: TicketUser;

    @HostBinding('style.border') get borderStyles() {
        return this.ticketUser && this.ticketUser.completed ? '1px solid green' : '1px solid #b2b2b2'
    }


    constructor(private ticketsService: TicketsService, public ticketsQuery: TicketsQuery, private cd: ChangeDetectorRef) { }

    ngOnInit(): void {
    }

    completeTicket() {
        this.ticketsService.completeTicket(this.ticketUser.id);
    }

    onAssignSelectChange(change: MatSelectChange) {
        this.ticketsService.assignTicket(this.ticketUser.id, change.value);
    }
}
