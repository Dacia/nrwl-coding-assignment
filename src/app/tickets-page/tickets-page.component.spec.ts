import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsPageComponent } from './tickets-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TicketItemModule } from './components/ticket-item/ticket-item.module';
import { TicketDetailModule } from './components/ticket-detail/ticket-detail.module';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { BackendService } from '../backend.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('TicketsPageComponent', () => {
    let component: TicketsPageComponent;
    let fixture: ComponentFixture<TicketsPageComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [TicketsPageComponent],
            providers: [
                {provide: BackendService, useValue: new BackendService()}
            ],
            imports: [
                BrowserAnimationsModule,
                CommonModule,
                RouterModule.forRoot([]),
                TicketItemModule,
                TicketDetailModule,
                MatIconModule,
                MatButtonModule,
                MatFormFieldModule,
                MatInputModule,
                FormsModule,
            ]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TicketsPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
