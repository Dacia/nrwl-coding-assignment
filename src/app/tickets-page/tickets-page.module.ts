import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketsPageComponent } from './tickets-page.component';
import { RouterModule, Routes } from '@angular/router';
import { TicketItemModule } from './components/ticket-item/ticket-item.module';
import { TicketDetailModule } from './components/ticket-detail/ticket-detail.module';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {path: 'tickets',  redirectTo: 'tickets/', pathMatch: 'full' },
    {path: 'tickets/:id', component: TicketsPageComponent},
];


@NgModule({
    declarations: [
        TicketsPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        TicketItemModule,
        TicketDetailModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
    ]
})
export class TicketsPageModule {}
