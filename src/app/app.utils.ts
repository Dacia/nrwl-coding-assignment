
export function pipeFuncs<T>(...fns: Array<(val: T) => T>): (initVal: T) => T {
    return (initVal: T) => fns.reduce((acc, val) => val(acc), initVal);
}
