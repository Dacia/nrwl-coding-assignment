import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TicketsPageModule } from './tickets-page/tickets-page.module';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'tickets/',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes/*, {enableTracing: true}*/),
        TicketsPageModule
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {}
